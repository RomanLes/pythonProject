#куда выводить данные GUI, WEB, MobilApp, Хранилище, Консоль, Терминал

# выводим одно слово
print('Hello')

# выводим несколько слов
print('Hello, world!')

# выводим любой набор символов
print('adsads $ asdasd dsadsa')

# выводим целое число
print(100)

# выводим число с плавающей точкой
print(90.89)

# выводим Истину
print(True)

# выводим Ложь
print(False)

# выводим None
print(None)

# New task

# строка целиком
print('Привет, меня зовут Кеша, мне 2 года')
# есть переменные
name = 'Кеша'
age = 2

# Как вывести строку вместе с переменными в терминал?
print(name, age)

# вывод через /
print(name, age, sep='/')
print(name, age, sep='/;h')

# end
print(name)
print(age)

print('-------------------------------------->')
print(name, end=';')
print(age, end=';')
print('end', end=';')