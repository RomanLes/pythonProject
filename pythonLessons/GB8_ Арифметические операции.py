# Арифметические операции

ale = 71
age = int(input('Скольок вам лет?: '))

#+
after20 = age + 20
print('Через 20 лет вам будет', after20)

#-
alive = ale - age
print('По статистике вам осталось жить', alive)

#*
count = 144000000
all_live = count * ale
print('Среднее время жизни всех людей', all_live)

#/
live_part = age / ale
print('Часть прожитой жизни', live_part)
#результат деления

print(type(live_part))