# GB_DZ3. Медицинская система

name = input('Введите своё имя: ')
#print('Имя:', name)

sure_name = input('Введите свою фамилию: ')
#print('Фамилия:', sure_name)

old = int(input('Введите свой возраст: '))
#print('Возраст:', old)

weight = int(input('Введите свой вес: '))
#print('Вес:', weight)

print('Ваши данные: ', name, sure_name, ':', 'возраст', old, 'вес', weight)

if old <= 30 and weight >= 50 and weight <= 120:
    print(name, sure_name, old, 'год\лет', 'вес', weight, ' - Пациент в хорошем состоянии!')
# Пациент в хорошем состоянии, если ему до 30 лет и вес от 50 и до 120 кг

elif old > 30 and old <= 40 and (weight < 50 or weight > 120):
    print(name, sure_name, old, 'год\лет', 'вес', weight, ' - Пациенту требуется заняться собой!')
# Пациенту требуется заняться собой, если ему более 30 и вес меньше 50 или больше 120 кг

elif old > 40 and (weight < 50 or weight > 120):
    print(name, sure_name, old, 'год\лет', 'вес', weight, ' - Пациенту требуется врачебный осмотр!')
# Пациенту требуется врачебный осмотр, если ему более 40 и вес менее 50 или больше 120 кг