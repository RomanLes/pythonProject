
# Условные операторы (if elif else)

age = int(input('Введите свой возраст: '))

if age < 18:
    print('Доступ запрещен')
    print('Доступ закрыт')
elif age == 18:
    print('Вам ровно 18 лет')
    print('Что с вами делать?')
elif age > 18 and age < 25:
    print('Отдельная категория пользоватлей')
else:
    print('Доступ разрешен')

print('Какие то действия')
print('end')