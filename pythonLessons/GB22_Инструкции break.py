# Инструкции break
# Цикл While. Задавать вопросы пока пользователь не введет правильный ответ

name = None

while True:
    name = input('Кто создатель python?')
    if name == 'Гвидо':
        break
    print('НЕ верно')

print('Верно!')