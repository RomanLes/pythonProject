# GB28_Функция len. Методы .find .split .isdigit .upper .lower

friends = 'Maxim Leonid'
print(friends)

# выясняем длину строки (исчисляется в символах)
print(len(friends))

# метод .find, ищем подстроку в строке. Методы вызываем через точку, *****.find
print(friends.find('Leo'))

# метод .find, ищем подстроку в строке. Добавили не существующую строку, 123
print(friends.find('123Leo'))

# метод .split, БЕЗ ПАРАМЕТРОВ!!! разбивает строку на части через пробел
print(friends.split())

# метод .split, С ПАРАМЕТРАМИ!!! (символ ; выступает разделителем строки) разбивает строку на части через пробел
friends = 'Maxim;Leonid'
print(friends.split(';'))

# метод .isdigit, проверяет состоит ли у нас строка только из цифер? Выдает False или True
print(friends.isdigit())

numder = '123'
print(numder.isdigit())

# метод .upper, Делает все буквы заглавными/большими
print(friends.upper())

# метод .lower, Делает все буквы маленькими
print(friends.lower())