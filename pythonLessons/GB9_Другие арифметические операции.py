# Другие арифметические операции

ale = 71
age = int(input('Скольок вам лет?: '))

#/
live_part = age / ale
print('Часть прожитой жизни', live_part)

#// - целая часть от деления
live_part = age // ale
print('Часть прожитой жизни', live_part)

# % остаток от деления
print(3%2)
print(4%2)

# ** возведение в степень
print(2**10)
print(2**2)
