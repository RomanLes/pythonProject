# Управляющие структуры
# x = 100
# if x == 10:
#     print('10!')
# elif x == 20:
#     print('20!')
# else:
#     print('Не знаю!')
#
# if x == 100:
#     print('x равно 100!')
#
# if x % 2 == 0:
#     print('x четное!')
# else:
#     print('x нечетное!')


# Выведите 3 различные строки
# print('Строка1', 'Строка2', 'Строка3', sep='\n')

# Поставить между словами знаки символы
# print('Строка1', 'Строка2', 'Строка3', sep=', ')

# Выведите 3 различные строки и добавить между ними пробелы
# print('Строка1', 'Строка2', 'Строка3', sep='\n\n')

# Передача переменной в функцию print()
# int_list = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
# print(int_list)

# Условные операторы, преобразование Строки в Число
# num = input('Введите число: ')
# if int(num) > 0:
#     if int(num) > 10:
#         print('Вы ввели число больше 10')
#         if int(num) > 50:
#             print('Вы ввели число больше 50')
#     else:
#         print('Вы ввели число меньше 10 и больше 0')
# elif int(num) == -10:
#     print('Вы ввели число меньше -10')
# else:
#     print('Вы ввели число меньше 0 и больше -10')
# print('All is okay!')

# Условные операторы
# name = input()
# A = 'Yes' if name != 'Test' else 'No'
# print(A)

# Циклы и Операторы для Циклов
# i = 1000
# while i > 100:
#     print(i)
#     i /= 2
#
# for j in 'hello world':
#     if j == 'a':  # оператор пропускает итерацию с буквой а
#         break  # выход из цикла на букве а
#     print(j * 2, end='')
# else:
#     print('Буквы а нет в слове')

# Пример реализации цикла while
# a = 10
# while True:  # Сперва выполняем цикл
#     a -= 1
#     if a == 0:  # Далее прописываем проверку
#         break

# Инструкции. Вывести строку 5 раз
# for i in range(5):
#     print('Hello world!')

# Выводить одно сообщение, если значение переменной меньше 10, и другое, если переменная больше или равна 10.
# i = int(input('Введите число: '))
# if i < 10:
#     print('Значение переменной Меньше 10')
# if i >= 10:
#     print('Значение переменной Больше или Равно 10')

# Выводить одно сообщение1, если i <= 10, сообщение2, если i > 10 and i <= 25, сообщение3, если i > 25
# i = int(input('Введите число: '))
# if i <= 10:
#     print('Значение переменной Меньше или Равно 10')
# elif (i > 10) and (i <= 25):
#     print('Значение переменной Больше 10 или Меньше Равно 25')
# elif i > 25:
#     print('Значение переменной Больше 25')

# Вариант1. Напишите программу, которая выполняет деление двух чисел и выводит остаток
# x = int(input('Введите число x: '))
# y = int(input('Введите число y: '))
# res = x % y
# print(res)

# Вариант2. Напишите программу, которая выполняет деление двух чисел и выводит остаток
# x = 5
# y = 2
# print(x % y)

# Напишите программу, которая принимает две переменные, делит одну на другую и выводит частное (результат)
# x = 5
# y = 2
# print(x / y)

# Выводить разные строки в зависимости от того, какое целое число было вами присвоено переменной age
# age = 13
# if age % 2 == 0:
#     print('Переменная Age, четное число!')
# else:
#     print('Переменная Age, нечетное число!')

# Вывести столбец четных чисел от 34 до 67
# i = 34
# while i <= 67:
#     if i % 2 != 1:
#         print(i)
#     i += 1

# Список (list)
# lis = [1, 56, 'x', 34, 2.34, ['S', 't', 'r', 'o', 'k', 'a']]  # Заполненый список
# print(lis)

# a = [a + b for a in 'list' if a != 's' for b in 'soup' if b != 'u']  # Объединяем буквы разных слов, с исключением
# print(a)
#
# x = [1, 2]
# x.append(23)  # Функция append добавляет элемент в конец списка
# x.append(34)  # Функция append добавляет элемент в конец списка
# b = [24, 67]
# x.extend(b)  # Функция extend расширяет один список другим списком
# x.insert(1, 56)  # Функция insert позволяет вставить элемент по индексу
# x.append(34)  # Функция append добавляет элемент в конец списка
# x.remove(34)  # Функция remove удаляет первый найденый элемент в списке имеющий заданное значение
# x.pop(0)  # Функция pop удаляет элемент по индексу. Если значение не задано, удаляет последний элемент списка
# print(x.index(24))  # Функция index показывает индекс искомого элемента списка
# print(x.count(34))  # Функция count показывает какое количество элементов в списке
# x.sort()  # Функция sort, сортирует список в порядке возрастания
# x.reverse()  # Функция reverse, переворачивает весь массив
# x.clear()  # Функция clear очищает весь список
# print(x)

# Индексы и срезы в списках
# l = [34, 'sd', 56, 34.34]  # Список
# print(l[0])  # Выводим на печать элемент списка по индексу (в [] указан индекс)
# print(l[-2])  # Выводим на печать элемент списка по индексу с конца (через минус) (в [] указан индекс)

# Выводим список в цикле
# l = [34, 'sd', 56, 34.34]
#
# i = 0  # Переменная равна 0
# while i < 4:  # Пока i < 4 работает цикл. Потом остановка цикла
#     print(l[i])  # Выводим на печать элемент списка по индексу
#     i += 1  # Увеличиваем i на 1
#
# item(START:STOP:STEP) Список, START == 0, STOP == длине объекта, STEP == 1
# Начало - с какого элемента стоит начать (по умолчанию равно 0);
# Конец - по какой элемент мы берем элементы (по умолчанию равно длине списка);
# Шаг - с каким шагом берем элементы, к примеру каждый 2 или 3 (по умолчанию каждый 1).
# print(l[:])  # Выводим список
# print(l[2:])  # Удаляем первые элемент в указаном количестве (с индекса 0...)
# print(l[:3])  # Удаляем элемент с индексом 3
# print(l[::2])  # Пропускаем каждый второй элемент (Берем каждый второй элемент)
# print(l[2::2]) # Начиная со второго элемента берем каждый второй элемент
# print(l[2:4:]) # Начиная с 4 элемента берем все элементы по 6 элемент
# print(l[::]) # Берем все элементы

# Кортежи (список который нельзя изменять), занимают меньше места. Определяются круглыми скобками ()
# a = (43, 56, 45.23, 'd')  # Кортеж
# b = [(]43, 56, 45.23, 'd']  # Список
# print(a.__sizeof__())  # Функция показывает сколько места в памяти занимает список или кортеж
# print(b.__sizeof__())  # Функция показывает сколько места в памяти занимает список или кортеж
#
# a = tuple('Hello world!')  # Деленый на элементы с помощью tuple. Работает с одним элементом
# print(a)

# Функции которые работают со списками, применимы и к Кортежам. Исключение функции которые изменяют Список.
# a = ('Hello world!', 'sdf', 345)  # Кортеж
# print(a)
# print(a.count(345))

# Словари и работа с ними. Способ 1
# d = {'test' : 1, 'test_2' : 'TeST'}
# print(d)

# Словари и работа с ними. Способ 2
# d = dict(short='dict', longer='dictionary')
# d['short'] = 234
# print(d)
#
# или так
# d = dict([(23, 34), (56, 87)])
# print(d)
#
# Словари и работа с ними. Способ 3
# d = dict.fromkeys(['a', 'b', 'c'], 1)
# print(d)
#
# Словари и работа с ними. Способ 4. С помощью Циклов
# d = {a: a ** 2 for a in range(7)}  # Увеличиваем а 7 раз
# print(d)

# Словари и работа с ними. Пример с данными человека
person = {'name': {'last_name': 'Иванов', 'first_name': 'Иван', 'middle_name': 'Иванович'},
          'address': ['г. Андрюшки', 'ул. Васильковская д. 23б', 'кв.12'],
          'phone': {'home_phone': '34-67-12', 'mobile_phone': '8-564-345-23-65', 'mobile_phone_2': 'Нет'}}
print(person['phone']['mobile_phone'])

# Список [], Кортеж (), Словарь {}
