# Инструкции continue
# Цикл While. Вывод НЕ четных чисел от 0 до n

number = 0
n = int(input('Введите n: '))

while number <= n:
    if number % 2 == 0: # если число делится на 2 без остатка выводим (только четные числа)
        number += 1 # аналог number = number + 1
        continue
    print(number)
    number += 1