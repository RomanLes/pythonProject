person_name = 'Max' # 'str' строка

print(type(person_name)) # команда для определния типа переменной

age = 30 # 'Int' целое число
print(type(age))

period = 3.2 # 'float' число с плавающей точкой
print(type(period))

is_good = True # 'bool' логический тип данных Правда\Ложь
print(type(is_good))

person = None # 'NoneType'
print(type(person))