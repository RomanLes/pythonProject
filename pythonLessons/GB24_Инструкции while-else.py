# Инструкции while-else
# Цикл While. Чисела от 0 до 100

number = 0

while number <= 100:
    print(number)
    number += 1
#    if number == 33: # если выполнить этот блок, команда else: не выполнится
#        break
else:           # else: выполнится только в том случае, когда выполниться while:
    print('end')

print('end') # команда для break