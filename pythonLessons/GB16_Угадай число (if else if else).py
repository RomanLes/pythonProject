# Угадай число (if else if else)

number = 43
value = int(input('Введите число от 1 до 100'))

if value == number:
    print('Вы угадали!')
else:
    if value > number:
        print('Загаданное число меньше')
    else:
        print('Загаданное число больше')