# GB_DZ2
# Используя цикл, запрашивайте у пользователя число, пока оно не станет больше 0, но меньше 10.
# После того, как пользователь введет корректное число, возведите его в степень 2 и выведите на экран.
#Например, пользователь вводит число 123, вы сообщаете ему, что число неверное, и говорите о диапазоне допустимых. И просите ввести заново.

# Альтернативное решение
#while True:
#   number = int(input('Введите число от 0 до 10: '))
#   if number > 0 and number < 10:
#       print(number ** 2)
#       break
#   else:
#       print('Число должно быть > 0 и < 10')

# Моё решение
number = int(input('Введите число от 0 до 10: '))

while number > 10 or number < 0:
    print('Введенное число выходит из заданного диапазана')
    number = int(input('Введите число от 0 до 10: '))

if number < 10:
    number = number ** 2
    print(number)