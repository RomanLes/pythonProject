# GB26_Строки. Определение. Методы. Форматирование.

friend = 'Maxim'

first_letter = friend[0]
print('Первая буква имени друга', first_letter)

first_letter = friend[1]
print('Вторая буква имени друга', first_letter)

first_letter = friend[2]
print('Третья буква имени друга', first_letter)

first_letter = friend[-1] # индексация с конца (получаем последнюю букву слова)
print('Последняя буква имени друга', first_letter)

print('Тип первого символа тоже строка:', type(first_letter))