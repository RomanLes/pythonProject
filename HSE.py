# Ввод-вывод и целочисленная арифметика, логические выражения и условный оператор
# !Работа с Числами

# print('Hello world!')
# print(2+3)
# print ('2'+'3')  # Тип данных строка
# print(2+3)
# print('2+3 = ', 2+3)
# print(2*3)
# print(2 // 3)  # Целочисленное деление

# Task1
# a = 123424242143132324326922342152
# b = 213732174232497412039472189472409
# c = a+b
# print('123424242143132324326922342152 + 213732174232497412039472189472409 =', c)


# print('абв', "ult", 'z', sep=',эээ,')

# print('1 + 2 + 3 + 4 = ', end='')
# print(1 + 2 + 3 + 4)

# Task2
# print('Питон', 'позволяет', 'автоматизировать', 'рутинные', 'задачи', sep=', ммм, ', end=', ммм, ')
# print('на', 'решение', 'которых', 'вручную', 'уходят', 'лучшие', 'годы', 'жизни', sep=', ммм, ')

# Task3
# print(12321482349287421983798237241*123721947294287497234957)  # Умножение
# print(2793042981421894210947210947//209734721987420234281947)  # Целочисленное деление

# v = 110  # Speed
# t = 11  # Time
# s = v * t  # Distance
# print(s, 'Km')

# Арифметические операции
# a = 11
# b = 5
# print(a, '+', b, '=', a + b)  # Сложение
# print(a, '-', b, '=', a - b)  # Минус
# print(a, '*', b, '=', a * b)  # Умножение
# print(a, '//', b, '=', a // b)  # Целочисленное деление
# print(a, '%', b, '=', a % b)  # Остаток от деления
# print(a, '**', b, '=', a ** b)  # Возведение в степень

# Task 4, по закону Ома U = IR
# i = 102341132412344234
# r = 9876387639476324727
# print(i, '*', r, '=', i * r)  # Умножение

# Task 5, разделить орехи на 60 белок
# a = 38746298762973632324233242
# b = 60
# print(a, '//', b, '=', a // b)  # Целочисленное деление

# Task 6, узнать сколько часов собержится в числе минут
# a = 38746298762973632324233242
# b = 60
# print(a, '//', b, '=', a // b)  # Целочисленное деление

# Task 7, узнать остаток орехов от 60 белок
# a = 38746298762973632324233242
# b = 60
# print(a, '%', b, '=', a % b)  # Остаток от деления

# Task 8, узнать куда показывает минутная стрелка
# a = 38746298762973632324233242
# b = 60
# print(a, '%', b, '=', a % b)  # Остаток от деления


# !Работа с Строками

# goodByePhrase = 'Asta la vista'  # Прощальная фраза
# person = 'baby'  # Кому она обращена
# print(goodByePhrase, person, sep=', ')

# str1 = 'abc'
# str2 = 'def'
# print(str1 + str2)

# str1 = 'abc'
# print(str1 * 3)

# number = 123456  # Тип Число
# string = '123456'  # Тип Строка
# print(number * 3, string * 3)

# number = 123456**2  # Число возведенное в степень
# string = str(number)  # Преобразуем Число в Строку
# print(string * 3)  # Повторяем число 3 раза

# number = 123456**2
# string = str(number)
# tripleStr = string * 3
# tripleInt = int(tripleStr)  # Из Строки делаем Число
# print(tripleInt // 2)  # Число дели на 2

# Task 9, узнать куда показывала минутная стрелка до полуночи
# a = -82372642987463918269721
# b = 60
# print(a, '%', b, '=', a % b)  # Остаток от деления


# Task 10, вывести 1000 раз символ А
# string = 'A'  # Строка
# print(string * 1000)  # Повторяем число 1000 раза

# Task 11, посчитать Число и записать Строку 10 раз
# number = 213169**123  # Число возведенное в степень
# string = str(number)  # Преобразуем Число в Строку
# print(string * 10)  # Повторяем число 10 раза

# Task 12, посчитать Число и записать Строку 10 раз и уполучившееся возвести в квадрат
# number = 213169**123  # Число возведенное в степень
# string = str(number)  # Преобразуем Число в Строку
# tripleStr = string * 10
# tripleInt = int(tripleStr)  # Из Строки делаем Число
# print(tripleInt**2)  # Число возводим в квадрат

# a = input()  # Введите число (тип Строка)
# b = input()  # Введите число (тип Строка)
# print(a + b)  # Выводит Строку

# a = int(input())  # Введите число (тип Целое число)
# b = int(input())  # Введите число (тип Целое число)
# print(a + b)  # Выводит сумму Чисел

# name = input()
# print('i love you', name)

# name = input()  # Вводим имя
# cnt = int(input())  # Вводим количество повторений имени
# print('i love you', name*cnt)

# Task 12, посчитать сколько кв. м. плитки необходимо купить
# a = int(input())
# b = int(input())
# print(a * b)

# Task 13, посчитать сколько погонных метров плинтуса необходимо купить
# a = int(input())
# b = int(input())
# print((a + b) * 2 )

# Task 14, посчитать сколько квадратных метров плитки необходимо купить
# a = int(input()) * 2
# b = int(input()) * 2
# c = int(input())
# print((a + b) * c)

# a = int(input())
# b = int(input())
# c = int(input())
# d = int(input())
# cost1 = a * 100 + b
# cost2 = c * 100 + d
# sumCost = cost1 + cost2
# print(sumCost // 100, sumCost % 100)  # Сначала выделяем рубли (целую часть), потом выделяем копейки

# m = int(input())  # Минутная стрелка
# t = int(input())  # Часовая стрелка
# print((m + t) % 60)  # Вычисляем положение часовой стрелки после 12:00

# Скрип по замене старых ценников на новые
# cost = int(input())
# print((cost - 1) // 10000 + 1)  # Окрушление вверх

# Task 15, вычислить стоиомость товара и умножить на 2
# a = int(input())
# b = int(input())
# cost1 = a * 100 + b
# sumCost = cost1 * 2
# print(sumCost // 100, sumCost % 100)  # Сначала выделяем рубли (целую часть), потом выделяем копейки

# Task 16, вычислить положение стрелок
# h = int(input())  # Часовая стрелка
# m = int(input())  # Минутная стрелка
# total1 = h * 60 + m
# print(total1 // 60 % 24, total1 % 60)

# Task 17, вычислить количество рулонов линолеума для кухни
# a = int(input())  # Длина помощения
# b = int(input())  # Ширина помощения
# c = int(input())  # Длина рулона
# d = 1  # Ширина рулона
# total1 = a * b
# total2 = c * d
# print((total1 - 1) // total2 + 1)

# Task 18, вычислить количество рулонов обоев для комнаты
# a = int(input()) * 2  # Длина помощения
# b = int(input()) * 2  # Ширина помощения
# c = int(input())  # Высота стены
# d = int(input())  # Длина рулона
# e = 1  # Ширина рулона
# total1 = (a + b) * c
# total2 = d * e
# print((total1 - 1) // total2 + 1)


# Условный оператор и цикл while

# Логические выражения not, and, or
# x = int(input())
# flag = x < 5
# print(flag)

# x = int(input())
# isEven = x % 2 == 0
# print(isEven)

# x = int(input())
# isEven = x % 2 == 0 and x > 10
# print(isEven)

# Значение х находится в пределах...
# x = int(input())
# flag = 3 <= x <= 10
# print(flag)

# Значение х находится в пределах... или
# x = int(input())
# flag = 3 <= x <= 10
# flag2 = flag or x % 7 == 3
# print(flag, flag2)

# Значение х находится в пределах... или
# x = int(input())
# flag = 3 <= x <= 10
# flag2 = not flag
# print(flag, flag2)

# Task 19, рабочее время магазина True or False
# x = int(input())
# flag1 = 6 <= x <= 8 or 16 <= x <= 17
# print(flag1)

# Task 20, рабочее время магазина True or False
# a = int(input())
# b = int(input())
# c = int(input())
# d = int(input())
# x = int(input())
# flag1 = a <= x <= b
# flag2 = not c <= x <= d
# flag3 = flag1 == flag2
# print(flag3)

# Преобразование логических типов
# flag = 5 < 3
# print(int(flag), int(not flag))

# flag = 5 < 3 or 1
# print(flag)
# print(int(flag), int(not flag))

# flag = 5 < 3
# print(str(flag) + str(not flag))

# print(bool(1))  # Все значения кроме 0, истина

# start1 = int(input())
# finish1 = int(input())
# start2 = int(input())
# finish2 = int(input())
# print(start1 <= finish2 and start2 <= finish1)

# Task 21, вычислить рабочие дни True or False
# x = int(input())
# w = x % 3 == True
# print(w)

# Task 22, вычислить рабочие дни True or False
# x = int(input())
# w = (x % 3) == True and (x % 4) == True
# print(w)

# Условный оператор if
# x = int(input())
# if x < 0:
#     x = -x
# print(x)

# x = int(input())
# if x < 0:
#     print(-x)
# if x >= 0:
#     print(x)

# x = int(input())
# if x < 0:
#     print(-x)
# if not (x < 0):
#     print(x)

# x = int(input())
# if x < 0:
#     print(-x)
# else:
#     print(x)

# x = int(input())
# y = int(input())
# if x > 0 and y > 0:
#     print(1)
# if x < 0 and y > 0:
#     print(2)

# x = int(input())
# y = int(input())
# if x > 0:
#     if y > 0:
#         print(1)
#     else:
#         print(4)
# else:
#     if y > 0:
#         print(2)
#     else:
#         print(3)

# Task 23, если ввели 1, вывести слово One
# x = int(input())
# if x == 1:
#     print('One')

# Task 24, если ввели 1, вывести слово One. Иное значение вывести слово Not one
# x = int(input())
# if x == 1:
#     print('One')
# else:
#     print('Not one')

# Task 25, Вывести большее значение
# x = int(input())
# y = int(input())
# if x > y:
#     print(x)
# else:
#     y > x
#     print(y)

# Операторы else и elif
# x = int(input())
# if x == 1:
#     print('one')
# else:
#     if x == 4:
#         print('four')
#     else:
#         print('other')

# x = int(input())
# if x == 1:
#     print('one')
# elif x == 4:
#     print('four')
# elif x == 11:
#     print('eleven')
# else:
#     print('other')

# Task 26, a > b p'1', b > a p'2', a == b p'0',
# a = int(input())
# b = int(input())
# if a > b:
#     print('1')
# elif b > a:
#     print('2')
# else:
#     print(0)

# Task 27, x > 0 p'1', x < 0 p'-1', x == 0 p'0'
# x = int(input())
# if x > 0:
#     print('1')
# elif x < 0:
#     print('-1')
# elif x == 0:
#     print('0')

# ! Task 28, вывести большее значение (a > b and a > c) p(a), (b > a and b > c) p(b), (c > a and c > b) p(c)
# a = int(input())
# b = int(input())
# c = int(input())
# if a >= b and a >= c:
#     print(a)
# elif b >= a and b >= c:
#     print(b)
# else:
#     c >= a and c >= b
#     print(c)

# Task 28, вычислить высокосный год. Кратен 4, но не кратен 100, а также если он кратен 400.
# x = int(input())
# if x == 100:
#     print('No')
# elif x % 4 == 0:
#     print('Yes')
# else:
#     print('No')

# Task 29, Выведите YES, есть ли среди них хотя бы одно четное и хотя бы одно нечетное, и NO в противном случае.
# a = int(input())
# b = int(input())
# c = int(input())
# if a % 2 == 0 and b % 2 == 0 and c % 2 == 0:
#     print('No')
# else:
#     if a % 2 != 0 and b % 2 != 0 and c % 2 != 0:
#         print('No')
#     else:
#         print('Yes')

# Task 30, По целому числу N распечатайте все квадраты натуральных (целых, положительных) чисел
# не превосходящие N, по возрастанию. Если N является полным квадратом, не печатать
# n = int(input())
# i = 1
# while i ** 2 <= n:
#     print(i ** 2)
#     i += 1

# Task 31, По числу N распечатайте все целые степени двойки, не превосходящие N, в порядке возрастания
# Если число N является степенью двойки, то его также необходимо вывести
# n = int(input())
# i = 1
# while i <= n:
#     i *= 2
#     print(i // 2)

# Определить сколько осадков в самый засушливый год
# now = int(input())
# minNum = now
# while now != 0:
#     if minNum > now:
#         minNum = now
#     now = int(input())
# print(minNum)

# Посчитать колличество засушливых лет (Счетчик минимумов)
# now = int(input())
# minNum = now
# cntMin = 1
# while now != 0:
#     if minNum > now:
#         minNum = now
#         cntMin = 1
#     elif now == minNum:
#         cntMin += 1
#     now = int(input())
# print(minNum, cntMin)  # выводит кол-во осадков и кол-во лет

# Task 32, Последовательность состоит из натуральных чисел, не превосходящих 109, и завершается числом 0.
# Определите значение наибольшего элемента последовательности.
# num = int(input())
# minNum = num
# while num != 0:
#     if minNum < num:
#         minNum = num
#     num = int(input())
# print(minNum)

# Task 33, Последовательность состоит из натуральных чисел и завершается числом 0
# Определите, сколько элементов этой последовательности больше предыдущего элемента

# numb = int(input())
# minNum = numb
# cntMin = 1
# while numb != 0:
#     if minNum > numb:
#         minNum = numb
#         cntMin = 1
#     elif numb == minNum:
#         cntMin += 1
#     numb = int(input())
# print(cntMin)

